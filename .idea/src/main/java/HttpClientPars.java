import sun.net.www.protocol.http.HttpURLConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class HttpClientPars {

    public String getDataAboutBus(String garageNumber) {

        String beginUrl = "http://data.kzn.ru:8082/api/v0/dynamic_datasets/bus/" + garageNumber + ".json";

        try {

            //http://data.kzn.ru:8082/api/v0/dynamic_datasets/bus.json вместо beginUrl работало
            URL url = new URL(beginUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();


            connection.setRequestProperty("Content-Type", "application/json");

            connection.setRequestMethod("GET");

            System.out.println(connection.getResponseCode());

            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            )) {
                StringBuilder content = new StringBuilder();
                String input;
                while ((input = reader.readLine()) != null) {
                    content.append(input);
                }
                connection.disconnect();
                return content.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Извините, но даже один пользователь для нашего сайта это много";
    }

}
